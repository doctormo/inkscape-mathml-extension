#! /usr/bin/env python

"""Command-line tool to replace MathML with SVG throughout a document.

Replaces all instances of MathML throughout the document"""

import sys
import os.path
from argparse import ArgumentParser
from xml import sax

from io import StringIO, BytesIO

from svgmath.tools.saxtools import XMLGenerator, ContentFilter
from svgmath.mathhandler import MathHandler, MathNS

def open_or_die(fname, fmode, role):
    try:
        return open(fname, fmode)
    except IOError as xcpt:
        sys.stderr.write("Cannot open %s file '%s': %s\n" % (role, fname, str(xcpt)))
        sys.exit(1)

class MathFilter (ContentFilter):
    def __init__(self, out, mathout):
        ContentFilter.__init__(self, out)
        self.plainOutput = out
        self.mathOutput = mathout
        self.depth = 0

    # ContentHandler methods
    def setDocumentLocator(self, locator):
        self.plainOutput.setDocumentLocator(locator)
        self.mathOutput.setDocumentLocator(locator)

    def startElementNS(self, elementName, qName, attrs):
        if self.depth == 0:
            (namespace, localName) = elementName
            if namespace == MathNS:
                self.output = self.mathOutput
                self.depth = 1
        else: self.depth += 1
        ContentFilter.startElementNS(self, elementName, qName, attrs)

    def endElementNS(self, elementName, qName):
        ContentFilter.endElementNS(self, elementName, qName)
        if self.depth > 0:
            self.depth -= 1
            if self.depth == 0:
                self.output = self.plainOutput

def main():
    parser = ArgumentParser(description="Generate from MathML")
    parser.add_argument("-f", "--formule", dest="formula", default="", help="MathML formula")
    parser.add_argument("input_file", nargs="?", metavar="INPUT_FILE",
                        help="Filename of the input file (default is stdin)", default=None)

    args = parser.parse_args()

    configfile = None
    encoding = 'utf-8'
    standalone = True

    if args.input_file:
        with open(args.input_file, 'rb') as fhl:
            svg = fhl.read()
    else:
        svg = sys.stdin.read()
    source = args.formula
    output = BytesIO()

    # Determine config file location
    if configfile is None:
        configfile = os.path.join(os.path.dirname(__file__), "svgmath.xml")
    config = open_or_die(configfile, "rb", "configuration")

    # Create the converter as a content handler.
    saxoutput = XMLGenerator(output, encoding)
    handler = MathHandler(saxoutput, config)
    if not standalone:
        handler = MathFilter(saxoutput, handler)

    try:
        parser = sax.make_parser()
        parser.setFeature(sax.handler.feature_namespaces, 1)
        parser.setContentHandler(handler)
        parser.parse(StringIO(source))
        # Quick and dirty insertion!
        sys.stdout.write(svg.replace(b'</svg>', b'').decode('utf8'))
        othersvg = output.getvalue().decode('utf8')
        othersvg = othersvg.split('</svg:metadata>')[-1].split('</svg:svg>')[0]
        sys.stdout.write('<g>')
        sys.stdout.write(othersvg)
        sys.stdout.write('</g>')
        sys.stdout.write('</svg>')
    except sax.SAXException as xcpt:
        sys.stderr.write("Error parsing input: %s\n" % (xcpt.getMessage()))
        sys.exit(1)
    sys.exit(0)

if __name__ == "__main__":
    main()
